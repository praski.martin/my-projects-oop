<?php
require ('Page.php');

$processTheComment = new Page();

//UTWORZENIE KRÓTKICH NAZW ZMIENNYCH
$name = trim($_POST['name']);  //trim usuwa znaki odstępu z początku i końca łańcucha
$email = trim($_POST['email']);
$comment = trim($_POST['comment']);

//ZDEFINIOWANIE DANYCH STATYCZNYCH
$addressTo = "praski.martin@gmail.com";
$topic = "Komentarz ze strony WWW";
$content = "Nazwa klienta: ".$name."\n".
    "Adres pocztowy: ".$email."\n".
    "Komentarz klienta: \n".str_replace("\r\n","",$comment)."\n"; //czysci tekst - zamienia \r\n na ""

mail($addressTo, $topic, $content);

$processTheComment->setParentType('Back-end');
$processTheComment->setTitle(' - Send E-mail');
$processTheComment->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$processTheComment->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$processTheComment->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$processTheComment->setBtns([
    "bakery.php" => "Piekarnia",
    "writeToFile.php" => "Zapis do pliku",
    "sendEMail.php" => "Wyślij e-mail"
]);
$processTheComment->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
<h1>Komentarz przyjęty</h1>
<p>Państwa komentarz (przedstawiony poniżej) został wysłany.</p>
<!--// nl2br zamienia znaki nowej linii jak \n na znak HTML-->
<!--nowej linii <br/>-->
<p>'.  nl2br(htmlspecialchars($content)).' </p>'
);
$processTheComment->display();