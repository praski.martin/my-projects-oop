<?php
require ('Page.php');

$bakery = new Page();

$bakery->setParentType('Back-end');
$bakery->setTitle(' - Bakery');
$bakery->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$bakery->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$bakery->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$bakery->setBtns([
    "bakery.php" => "Piekarnia",
    "writeToFile.php" => "Zapis do pliku",
    "comment.html" => "Wyślij e-mail"
]);
$bakery->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
            <h5>System zniżek dla zamówień na duże ilości bułek:<br/>
                <ul>
                    <ol>zamówienie mniej niż 10 bułek - brak zniżki</ol>
                </ul>
                <ul>
                    <ol>zamówienie od 10 do 49 bułek - zniżka 5%</ol>
                </ul>
                <ul>
                    <ol>zamówienie od 50 do 99 bułek - zniżka 10%</ol>
                </ul>
                <ul>
                    <ol>zamówienie powyżej 100 bułek - zniżka 15%</ol>
                </ul>
            </h5>

            <!--FORM -->
            <form action="processTheOrder.php" method="post">
                <table style="border:0;">
                    <tr style="background: #cccccc">
                        <td style="width: 150px; text-align: center">Produkt</td>
                        <td style="width: 15px; text-align: center">Cena</td>
                        <td style="width: 15px; text-align: center">Ilość</td>
                    </tr>
                    <tr>
                        <td>Chleb</td>
                        <td>4 PLN</td>
                        <td><input type="text" name="amountOfBread" size="3" maxlength="3"></td>
                    </tr>
                    <tr>
                        <td>Bułka</td>
                        <td>0,7 PLN</td>
                        <td><input type="text" name="numberOfRolls" size="3" maxlength="3"></td>
                    </tr>
                    <tr>
                        <td>Pączek</td>
                        <td>1,7 PLN</td>
                        <td><input type="text" name="numberOfDonuts" size="3" maxlength="3"></td>
                    </tr>
                    <tr>
                        <td>Jak dowiedzieli się państwo o naszej piekarni?</td>
                        <td><select name="source">
                            <option value="a">Jestem stałym klientem</option>
                            <option value="b">Reklama telewizyjna</option>
                            <option value="c">Książka telefoniczna</option>
                            <option value="d">Znajomy</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align:center;"><input type="submit" value="Złóż zamówienie"> </td>
                    </tr>
                </table>
            </form>
');
$bakery->display();