<?php


class page
{
    private $title = "Moja strona OOP";
    private $metaTag ="width=device-width, initial-scale=1";
    private $styleSheet =[[
        "rel" => "stylesheet",
        "type" => "text/css",
        "href" => "style.css"
    ]];
    private $scripts =[
        "https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"
    ];

    private $btns =[
        "#" =>"Link 1",
    ];

    private $parentType = '';

    private $content='';

    public function display()
    {
        echo "<html lang='en'>\n<head>\n";
        $this->displayTitle();
        echo "<meta charset=\"utf-8\">\n";
        $this->displayMetaTag();
        $this->displayStyle();
        $this->displayScript();
        echo "</head>\n<body>\n";
        echo "<!--NAVBAR-->
        <nav class=\"navbar navbar-expand-sm bg-secondary fixed-\">
            <a class=\"navbar-brand colorNavbar\" href=\"#\">Navbar</a>
            <button class=\"navbar-toggler navbar-dark\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">
                <span class=\"navbar-toggler-icon \"></span>
            </button>
            <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">
                <ul class=\"navbar-nav \">
                    <li class=\"nav-item \">
                        <a  class=\"nav-link connect";
        if ($this->getFileNameByTitle() === 'index.php') {
            echo ' dOrange';
        } else {
            echo ' whiteLetters';
        }
        echo "\" href=\"index.php\">Home</a>
                    </li>
                    <li class=\"nav-item\">
                        <a id=\"front\" class=\"nav-link connect";
        if ($this->getFileNameByTitle() === 'frontEnd.php' || $this->parentType === 'Front-end') {
            echo ' dOrange';
        } else {
            echo ' whiteLetters';
        }
        echo " \" href=\"frontEnd.php\">Front-end</a>
                    </li>
                    <li class=\"nav-item\">
                        <a id=\"back\" class=\"nav-link connect";
        if ($this->getFileNameByTitle() === 'backEnd.php' || $this->parentType === 'Back-end') {
            echo ' dOrange';
        } else {
            echo ' whiteLetters';
        }
        echo "\" href=\"backEnd.php\">Back-end</a>
                    </li>
                </ul>
            </div>
        </nav>
        <!--CONTAINER-->
        <div class=\"container-fluid\">
            <div class=\"row\">
                <div class=\"col-sm-2\">
                    <nav class=\"navbar bg-secondary \">
                        <ul class=\"navbar-nav\">\n";
                            $this->displaySideBars();
                        echo "
                        </ul>
                    </nav>
                </div>
                <div class=\"col-sm-10\">";
                    echo $this->content;
                echo"</div>
            </div>
        </div>
        \n</body>\n</html>\n";
    }

    private function displayTitle()
    {
        echo "<title> $this->title </title>\n";
    }

    private function displayMetaTag()
    {
        echo "<meta name=\"viewport\" content=\"$this->metaTag\">\n";
    }

    private function displayStyle()
    {
        $styles='';
        foreach ($this->styleSheet as $sheet)
        {
            $styles.='<link ';
            foreach ($sheet as $key => $value )
            {
                $styles.=  $key ."=\"$value\" " ;
            }
            $styles.=">\n";
        }
        echo $styles;
    }

    private function displayScript()
    {
        $src='';
        foreach ($this->scripts as $script)
        {
            $src.= "<script src=\"".$script."\"></script>\n";
        }
        echo $src;
    }

    public function displaySideBars()
    {
        $link="";
        foreach ($this->btns as $key => $value)
        {
            $link.=$this->additionalClass($key,$value);
        }
        $linkFixed =substr($link,0,-1);
        echo $linkFixed;
    }

    public function additionalClass($key,$value)
    {
        $link = "\t\t\t\t\t\t\t<li class=\"nav-item \">\n\t\t\t\t\t\t\t\t<a class=\"nav-link";
        if ($this->getFileNameByTitle() === $key) {
            $link .= ' dOrange';
        } else {
            $link .= ' whiteLetters';
        }
        $link .= "\" href=\"".$key."\">".$value."</a>\n\t\t\t\t\t\t\t</li>\n";
        return $link;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $this->parentType . $title;
    }

    /**
     * @param array $styleSheet
     */
    public function addStyleSheet(array $styleSheet): void
    {
            $this->styleSheet[] = $styleSheet;
    }

    /**
     * @param string $script
     */
    public function addScript(string $script): void
    {
        $this->scripts[] = $script;
    }

    /**
     * @param array $btns
     */
    public function setBtns(array $btns): void
    {
        $this->btns = $btns;
    }

    /**
     * @param string $parentType
     */
    public function setParentType(string $parentType): void
    {
        $this->parentType = $parentType;
    }

    private function getFileNameByTitle()
    {
        switch ($this->title) {
            case 'Moja strona OOP':
                return 'index.php';
            case 'Front-end':
                return 'frontEnd.php';
            case 'Back-end':
                return 'backEnd.php';
            case $this->parentType . ' - Select number':
                return 'selectNumber.php';
            case $this->parentType . ' - Memory':
                return 'memory.php';
            case $this->parentType . ' - Mushroom Picking':
                return 'mushroomPicking.php';
            case $this->parentType . ' - Blind Date':
                return 'blindDate.php';
            case $this->parentType . ' - Word Search':
                return 'wordSearch.php';
            case $this->parentType . ' - Bakery':
                return 'bakery.php';
            case $this->parentType . ' - Write To File':
                return 'writeToFile.php';
            case $this->parentType . ' - Send E-mail':
                return 'sendEMail.php';
        }
    }

    /**
     * @param mixed $content
     */
    public function setContent($content): void
    {
        $this->content = $content;
    }

}