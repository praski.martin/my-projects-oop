<?php
require ('Page.php');
//tworze krótkie nazwy zmiennych
$amountOfBread = $_POST['amountOfBread'] ?:0;
$numberOfRolls = $_POST['numberOfRolls'] ?:0;
$numberOfDonuts = $_POST['numberOfDonuts'] ?:0;
$source = $_POST['source'];

$quantity = $amountOfBread + $numberOfRolls + $numberOfDonuts;
$result='';
$dateOfMakingOrder = '<p>Zamówienie przyjęte o '. date('H:i, jS F Y').'</p>';
if ($dateOfMakingOrder < $quantity){
    $result.= $dateOfMakingOrder;
}

if ($numberOfRolls < 10 )
    $discount = 1;
elseif ($numberOfRolls >= 10 && $numberOfRolls <= 49 )
    $discount = 0.95;
elseif ($numberOfRolls >= 50 && $numberOfRolls <= 99)
    $discount = 0.9 ;
elseif ($numberOfRolls >= 100)
    $discount = 0.85;

if ($quantity==0){
    $result.= "Na poprzedniej stronie nie zostało złożone żadne zamówienie! <br/>";

}else{
        $result.= "Zamówiono: </br>";

    if ($amountOfBread > 0)
        $result .= htmlspecialchars($amountOfBread) . ' chleba <br/>';
    if ($numberOfRolls > 0)
        $result .= htmlspecialchars($numberOfRolls) . ' bułek <br/>';
    if ($numberOfDonuts > 0)
        $result .= htmlspecialchars($numberOfDonuts) . ' pączków <br/><br/>';

    $result .= "</br>";

    $value = 0.00;

    define('BREADPRICE', 4);
    define('PRICEOFROLLS', 0.7);
    define('PRICEOFDONUTS', 1.7);

    $value = ($numberOfRolls * $discount) * PRICEOFROLLS
        + $amountOfBread * BREADPRICE
        + $numberOfDonuts * PRICEOFDONUTS;

    $result .= 'Cena netto: ' . number_format($value, 2) . "PLN<br/>";

    $vatRate = 0.22; // stawka vat wynosi 22%
    $value = $value * (1 + $vatRate);
    $result .= "Cena brutto: " . number_format($value, 2) . "PLN</p>";

    $result .= "Źródło informacji: \n ";
    switch ($source) {
        case "a":
            $result .= "Stały klient";
            break;
        case "b":
            $result .= "Reklama telewizyjna";
            break;
        case "c":
            $result .= "Książka telefoniczna";
            break;
        case "d":
            $result .= "Znajomy";
            break;
        default:
            $result .= "Źródło nieznane";
            break;
    }
}
$processTheOrder = new Page();

$processTheOrder->setParentType('Back-end');
$processTheOrder->setTitle(' - Bakery');
$processTheOrder->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$processTheOrder->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$processTheOrder->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$processTheOrder->setBtns([
    "bakery.php" => "Piekarnia",
    "writeToFile.php" => "Zapis do pliku",
    "sendEMail.php" => "Wyślij e-mail"
]);

$processTheOrder->setContent('
        <div class="col-sm-10">
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
            <h1>Piekarnia</h1>
            <h2>Wyniki zamówienia :</h2>
'.$result);
$processTheOrder->display();








