<?php
require ('Page.php');

$writeToFile = new Page;

$writeToFile->setParentType('Back-end');
$writeToFile->setTitle(' - Write To File');
$writeToFile->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$writeToFile->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$writeToFile->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$writeToFile->setBtns([
    "bakery.php" => "Piekarnia",
    "writeToFile.php" => "Zapis do pliku",
    "sendEMail.php" => "Wyślij e-mail"
]);
$writeToFile->setContent('
           <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
            <div class="d-flex ">
                <div class="p-2 ml-auto ">
                    <a href="seeOrders.php" ><button type="button" class="btn btn-info" >Zobacz zamówienia</button></a>
                </div>
            </div>
            <h5>System zniżek dla zamówień na duże ilości bułek:<br/>
                <ul>
                    <ol>zamówienie mniej niż 10 bułek - brak zniżki</ol>
                </ul>
                <ul>
                    <ol>zamówienie od 10 do 49 bułek - zniżka 5%</ol>
                </ul>
                <ul>
                    <ol>zamówienie od 50 do 99 bułek - zniżka 10%</ol>
                </ul>
                <ul>
                    <ol>zamówienie powyżej 100 bułek - zniżka 15%</ol>
                </ul>
            </h5>

            <form action="processTheOrderToFile.php" method="post">
                <table style="border:0; text-align: center">
                    <tr style="background: #cccccc">
                        <td style="width: 150px">Produkt</td>
                        <td style="width: 15px">Ilość</td>
                    </tr>
                    <tr>
                        <td >Chleb</td>
                        <td><input type="text" name="amountOfBread" size="3" maxlength="3"></td>
                    </tr>
                    <tr>
                        <td>Bulki</td>
                        <td><input type="text" name="numberOfRolls" size="3" maxlength="3"></td>
                    </tr>
                    <tr>
                        <td>Paczki</td>
                        <td><input type="text" name="numberOfDonuts" size="3" maxlength="3"></td>
                    </tr>
                    <tr>
                        <td>Adres</td>
                        <td><input type="text" name="address" size="30" maxlength="50"></td>
                    </tr>
                    <tr>
                        <td>Jak dowiedzieli się państwo o naszej piekarni?</td>
                        <td><select name="source">
                            <option value="a">Jestem stałym klientem</option>
                            <option value="b">Reklama telewizyjna</option>
                            <option value="c">Książka telefoniczna</option>
                            <option value="d">Znajomy</option>
                        </select>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" ><input type="submit" value="Złóż zamówienie"> </td>
                    </tr>
                </table>
            </form>
');
$writeToFile->display();