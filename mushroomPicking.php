<?php
require ('Page.php');

$mushroomPicking = new Page();

$mushroomPicking->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$mushroomPicking->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$mushroomPicking->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$mushroomPicking->setParentType('Front-end');
$mushroomPicking->setTitle(' - Mushroom Picking');
$mushroomPicking->setBtns([
    "selectNumber.php" =>"Losowanie liczby 1-9",
    "memory.php" =>"Memory",
    "mushroomPicking.php" =>"Zbieranie grzybów",
    "blindDate.php" =>"Randka w ciemno",
    "wordSearch.php" =>"Szukanie słowa"
]);
$mushroomPicking->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2  ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">js</div>
            </div>
            <div  id="content2">
                <!--Masz 6 rodzajów grzybów w tym muchomor.Losujesz pięć. Grzyby podczas losowania nie mogą sie powtarzać. Jeżeli natrafisz-->
                <!--na muchomora kończysz zbieranie.-->
                <button id="myBtn" type="button" class="btn btn-success" onclick="myFunction()">
                    Kliknij aby zacząć zbierać grzyby !
                </button>
                <button id="info" type="button" class="btn btn-info d-none" onclick="myFunction1()">Spróbuj jeszcze raz</button>
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Zebrane grzyby:<span id="how_many_gathered"></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="mushrooms">brak grzybów</td>
                        </tr>
                        </tbody>
                    </table>
            </div>
            <script src="js/mushroomPicking.js"></script>
');
$mushroomPicking->display();