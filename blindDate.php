<?php
require ('Page.php');

$blindDate = new Page();

$blindDate->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$blindDate->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$blindDate->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$blindDate->setParentType('Front-end');
$blindDate->setTitle(' - Blind Date');
$blindDate->setBtns([
    "selectNumber.php" =>"Losowanie liczby 1-9",
    "memory.php" =>"Memory",
    "mushroomPicking.php" =>"Zbieranie grzybów",
    "blindDate.php" =>"Randka w ciemno",
    "wordSearch.php" =>"Szukanie słowa"
]);
$blindDate->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">js</div>
            </div>
            <div class="bg-danger text-light p-3">
                <h3>Podaj pięć imion damskich  oraz pięć imion męskich aby rozpocząć losowanie.</h3>
                <div class="row">
                    <form class="col text-justify" id="formularz" name="myForm" >
                        <label for="name1">
                            <strong>Imię kobiety:</strong>
                        </label><br>
                        <input id="name1" type="text" name="woman" value="" ><span id="comment1"></span>
                        <br><br>
                        <label for="name2">
                            <strong>Imię mężczyzny:</strong>
                        </label><br>
                        <input id="name2" type="text" name="man" value="" ><span id="comment2"></span>
                        <br><br>
                        <button class="btn btn-deafult" id="login" type="submit" value="Submit" >Wyślij</button>
                    </form>
                    <p class="col-6" id="demo"></p>
                </div>
            </div>
            <script src="js/blindDate.js"></script>
');
$blindDate->display();
