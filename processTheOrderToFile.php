<?php
require ('Page.php');

//krótkie nazwy zmiennych
$amountOfBread = $_POST['amountOfBread'] ?:0;
$numberOfRolls = $_POST['numberOfRolls'] ?:0;
$numberOfDonuts = $_POST['numberOfDonuts'] ?:0;
$source = $_POST['source'];

$quantity = $amountOfBread + $numberOfRolls + $numberOfDonuts;
$result='';
$dateOfMakingOrder = '<p>Zamówienie przyjęte o '. date('H:i, jS F Y').'</p>';
if ($dateOfMakingOrder < $quantity){
    $result .= $dateOfMakingOrder;
}

if ($numberOfRolls < 10 )
    $discount = 1;
elseif ($numberOfRolls >= 10 && $numberOfRolls <= 49 )
    $discount = 0.95;
elseif ($numberOfRolls >= 50 && $numberOfRolls <= 99)
    $discount = 0.9 ;
elseif ($numberOfRolls >= 100)
    $discount = 0.85;

if ($quantity==0){
    $result .= "Na poprzedniej stronie nie zostało złożone żadne zamówienie! <br/>";

}else {
    $result .= "Zamówiono: </br>";

    if ($amountOfBread > 0)
        $result .= htmlspecialchars($amountOfBread) . ' chleba <br/>';
    if ($numberOfRolls > 0)
        $result .= htmlspecialchars($numberOfRolls) . ' bułek <br/>';
    if ($numberOfDonuts > 0)
        $result .= htmlspecialchars($numberOfDonuts) . ' pączków <br/><br/>';

    $result .= "</br>";

    $value = 0.00;

    define('BREADPRICE', 4);
    define('PRICEOFROLLS', 0.7);
    define('PRICEOFDONUTS', 1.7);

    $value = $amountOfBread * BREADPRICE
        + ($numberOfRolls * $discount) * PRICEOFROLLS
        + $amountOfBread * PRICEOFDONUTS;

    $result .= "Cena netto: " . number_format($value, 2) . "PLN<br/>";

    $vatRate = 0.22; // stawka vat wynosi 22%
    $value = $value * (1 + $vatRate);
    $result .= "Cena brutto: " . number_format($value, 2) . "PLN</p>";

    $result .= "Źródło informacji:";
    switch ($source) {
        case "a":
            $result .= "<p>Stały klient</p>";
            break;
        case "b":
            $result .= "<p>Reklama telewizyjna</p>";
            break;
        case "c":
            $result .= "<p>Książka telefoniczna</p>";
            break;
        case "d":
            $result .= "<p>Znajomy</p>";
            break;
        default:
            $result .= "<p>Źródło nieznane<p/>";
            break;
    }

// Zapisywanie do pliku

    $address = $_POST['address'];
    $data = date('H:i, jS F Y');
    $document_root = $_SERVER['DOCUMENT_ROOT'];

    $outputString = $data . "\t" . $amountOfBread . " chleba \t" . $numberOfRolls . " bułek\t"
        . $numberOfDonuts . " pączków\t" . number_format($value, 2)
        . "PLN\t" . $address . "\n";

    $wp = fopen("$document_root/gitlab/my_page_OOP/orders.txt", 'ab');

    if (!$wp) {
        echo "<p><strong>Zamówienie państwa nie może zostać przyjęte w tej chwili.
        Proszę spróbować później.</strong></p>";
        exit;
    }

    fwrite($wp, $outputString);
    fclose($wp);
}

$processTheOrderToFile = new Page;

$processTheOrderToFile->setParentType('Back-end');
$processTheOrderToFile->setTitle(' - Write To File');
$processTheOrderToFile->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$processTheOrderToFile->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$processTheOrderToFile->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$processTheOrderToFile->setBtns([
    "bakery.php" => "Piekarnia",
    "writeToFile.php" => "Zapis do pliku",
    "sendEMail.php" => "Wyślij e-mail"
]);
$processTheOrderToFile->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2  ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
            <div class="d-flex ">
                <div class="p-2 ml-auto ">
                    <a href="seeOrders.php" ><button type="button" class="btn btn-info" >Zobacz zamówienia</button></a>
                </div>
                <div class="p-2 ">
                    <a href="writeToFile.php" ><button type="button" class="btn btn-info" >Złóż zamówienie</button></a>
                </div>
            </div>
            <h1>Piekarnia</h1>
            <h2>Wyniki zamówienia :</h2>
'.$result);
$processTheOrderToFile->display();