<?php
require ('Page.php');

$selectNumber = new Page();

$selectNumber->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$selectNumber->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$selectNumber->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$selectNumber->setParentType('Front-end');
$selectNumber->setTitle(' - Memory');
$selectNumber->setBtns([
    "selectNumber.php" =>"Losowanie liczby 1-9",
    "memory.php" =>"Memory",
    "mushroomPicking.php" =>"Zbieranie grzybów",
    "blindDate.php" =>"Randka w ciemno",
    "wordSearch.php" =>"Szukanie słowa"
]);
$selectNumber->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2  ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">js</div>
            </div>
            <h1>Ilość ruchów:<span id="ruchy"></span></h1>
            <div class="card-group ">
                <div class="card bg-primary ">
                    <div id="0"  class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="1"  class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="2"  class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="3"  class="card-body text-center card-text to-open">?</div>
                </div>
            </div>
            <div class="card-group">
                <div class="card bg-primary">
                    <div id="4"  class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="5"  class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="6"  class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="7"  class="card-body text-center card-text to-open">?</div>
                </div>
            </div>
            <div class="card-group">
                <div class="card bg-primary">
                    <div id="8"  class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="9"  class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="10" class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="11" class="card-body text-center card-text to-open">?</div>
                </div>
            </div>
            <div class="card-group">
                <div class="card bg-primary">
                    <div id="12" class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="13" class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="14" class="card-body text-center card-text to-open">?</div>
                </div>
                <div class="card bg-primary">
                    <div id="15" class="card-body text-center card-text to-open">?</div>
                </div>
            </div>
            <button id="swap" class="btn-success button" onclick="location.reload();"> Zagraj ponownie</button>
                                    <script src="js/memory.js"></script>
            ');
$selectNumber->display();