<?php
require ('Page.php');

$wordSearch = new Page();

$wordSearch->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$wordSearch->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$wordSearch->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$wordSearch->setParentType('Front-end');
$wordSearch->setTitle(' - Word Search');
$wordSearch->setBtns([
    "selectNumber.php" =>"Losowanie liczby 1-9",
    "memory.php" =>"Memory",
    "mushroomPicking.php" =>"Zbieranie grzybów",
    "blindDate.php" =>"Randka w ciemno",
    "wordSearch.php" =>"Szukanie słowa"
]);
$wordSearch->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2  ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">js</div>
            </div>
            <div class="p-2">
                <div class="d-flex h-10">
                    <div class="align-self-center mx-auto ">
                        <button id="button" class="bg-info"  type="button"  onclick="">Losuj wyraz</button>
                    </div>
                </div>
                <h3></h3>
                <button id="button2" class="d-none bg-info"  onclick="location.reload();">Zagraj ponownie</button>
                <span id="how_many_tries"></span>
                <div id="frame"></div>
                <input id="text" type="text" class="d-none" ><span id="show"></span>
                <div id="frame2" ></div>
            </div>
            <script src="js/wordSearch.js"></script>
');
$wordSearch->display();
