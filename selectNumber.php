<?php
require ('Page.php');

$selectNumber = new Page();

$selectNumber->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$selectNumber->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$selectNumber->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$selectNumber->setParentType('Front-end');
$selectNumber->setTitle(' - Select number');
$selectNumber->setBtns([
    "selectNumber.php" =>"Losowanie liczby 1-9",
    "memory.php" =>"Memory",
    "mushroomPicking.php" =>"Zbieranie grzybów",
    "blindDate.php" =>"Randka w ciemno",
    "wordSearch.php" =>"Szukanie słowa"
]);
$selectNumber->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2  ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">js</div>
            </div>
            <div id="content1">
                <h2 class="aaa">Wyszukaj liczbę z przedziału 1-9:</h2>
                <input id="input" type="number"  value="">
                <h3 id="information"></h3>
                <button type="button" id="again" class="d-none" onclick="location.reload()">Zagraj ponownie</button>
                        <script src="js/selectNumber.js"></script>
            </div>
            ');
$selectNumber->display();