<?php
require('Page.php');

$sendEMail = new Page();

$sendEMail->setParentType('Back-end');
$sendEMail->setTitle(' - Send E-mail');
$sendEMail->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$sendEMail->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$sendEMail->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$sendEMail->setBtns([
    "bakery.php" => "Piekarnia",
    "writeToFile.php" => "Zapis do pliku",
    "sendEMail.php" => "Wyślij e-mail"
]);
$sendEMail->setContent('
           <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2 ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
<h1>Komentarz klienta</h1>
<p>Proszę przekazać nam swoje komentarze</p>

<form action="processTheComment.php " method="post">
    <p>Nazwisko:<br/>
        <input type="text" name="name" size="40"/></p>

    <p>Adres poczty elektronicznej:<br/>
        <input type="text" name="email" size="40"/></p>

    <p>Komentarz:<br/>
        <textarea name="comment" cols="40" rows="8" ></textarea></p>

    <p><input type="submit" value="Wyślij komentarz" /></p>
</form>
');
$sendEMail->display();