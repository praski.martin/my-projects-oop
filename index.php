<?php
require("Page.php");

$home = new Page();

$home->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$home->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$home->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');

$home->display();

