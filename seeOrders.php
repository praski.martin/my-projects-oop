<?php
require ('Page.php');

$seeOrders = new Page;

$document_root = $_SERVER['DOCUMENT_ROOT'];

$wp = fopen("$document_root/gitlab/my_page_OOP/orders.txt", 'rb');
flock($wp,LOCK_SH);   //LOCK_SH blokada odczytu pliku, możliwość korzystania z pliku z innymi czytającymi

if (!$wp){
    $result .= "<p><strong>Brak zamówień.<br/>
        Proszę spróbować później.</strong></p>";
    exit;
}
$result='';
while (!feof($wp)){
    $order = fgets($wp);
    $result .= htmlspecialchars($order)."<br/>";
}
flock($wp,LOCK_UN);  //zwolnienie blokady pliku
fclose($wp);

$seeOrders->setParentType('Back-end');
$seeOrders->setTitle(' - Write To File');
$seeOrders->addStyleSheet(['rel'=>'stylesheet','href'=>'https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css']);
$seeOrders->addScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js');
$seeOrders->addScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js');
$seeOrders->setBtns([
    "bakery.php" => "Piekarnia",
    "writeToFile.php" => "Zapis do pliku",
    "sendEMail.php" => "Wyślij e-mail"
]);
$seeOrders->setContent('
            <div class="d-flex mb-3 border-bottom">
                <div class="p-2  ">html</div>
                <div class="p-2  ">css</div>
                <div class="p-2 ">bootstrap</div>
                <div class="p-2 ">php</div>
            </div>
            <div class="d-flex ">
                <div class="p-2 ml-auto ">
                    <a href="writeToFile.php" ><button type="button" class="btn btn-info" >Złóż zamówienie</button></a>
                </div>
            </div>
            <h1>Piekarnia</h1>
            <h2>Zamówienia klientów</h2>
'.$result);
$seeOrders->display();