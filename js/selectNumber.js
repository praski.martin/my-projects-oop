var compartment = [1,2,3,4,5,6,7,8,9];
var my_number = Math.floor(Math.random() * compartment.length);
var number= compartment[my_number];
var information = $("#information");

$("input").on('change', function () {

    var number1= $("input").val();

    if (number== number1 ){
        information.html("Wylosowałeś szukaną liczbe");
        $('input').attr('readonly',true);
        $("#again").removeClass('d-none');
    }else if( number >number1){
        information.html("Wylosowana liczba jest za mała");
    }else if ( number < number1 && number1 < 10){
        information.html("Wylosowana liczba jest za duża");
    }else{
        information.html("Podaj liczbę z przedziału 1-9");
    }
});